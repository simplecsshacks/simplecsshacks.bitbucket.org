(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";

function phonegapBoot()
{
    $(".browsing-mode").html("<span class='im im-phone3'></span> Phonegap Mode");
}

var ranOnce = false;

// This was made for the cases where libraries would be seperate from site code
// and both loaded asynchronusly. One or the other would load first due to race-conditions.
// This fixed that by attaching a special code at the end of both files to call runOnce.

// This is no longer the case however as all files are compacted to one file, but this is
// kept in anyways in case the files are split sometime in the future again
function runOnce()
{
    //console.log("runOnce called");

    if(!ranOnce) ranOnce = true;
    else return;

    $(".social-share .facebook").click(require("./social/facebook").socialCycleFB);
    $(".social-share .twitter").click(require("./social/twitter").socialCycleTwitter);
    $(".social-share .google").click(require("./social/google-plus").socialCycleGoogle);
    $(".social-share .pintrest").click(require("./social/pintrest").socialCyclePintrest);

    require("./util/chikta-ads").loadChiktaAdsYet(true);
    require("./util/disqus").loadDisqusYet(true);
    require("./social/code-pen").loadCodePenYet(true);
    require("./social/youtube").loadYoutubeYet(true);

    require("./lib/foundation").reloadFoundation();
    require("./util/analytics").analytics();
    require("./lib/phonegap").loadPhonegapScript();

    if(
        window.nwDispatcher &&
        window.nwDispatcher.requireNwGui) $(".browsing-mode").html("<span class='im im-desktop2'></span> Node-Webkit Mode");
    else $(".browsing-mode").html("<span class='im im-browser3'></span> Web Browser Mode");

    // Listen to this to detect if we're running inside phonegap or not
    $(document).on("deviceready", phonegapBoot);
}

window.libsReady = function()
{
    runOnce();
};

if(typeof $ != 'undefined')
{
    $(function()
    {
        //console.log("jQuery onReady called");
        runOnce();
    });
}

},{"./lib/foundation":2,"./lib/phonegap":3,"./social/code-pen":6,"./social/facebook":7,"./social/google-plus":8,"./social/pintrest":9,"./social/twitter":10,"./social/youtube":11,"./util/analytics":12,"./util/chikta-ads":13,"./util/disqus":14}],2:[function(require,module,exports){
"use strict";

// Reloads foundation on website
// Documentation can be found at: http://foundation.zurb.com/docs
exports.reloadFoundation = function()
{
    // Reload all of foundation
    $(document).foundation();

    // According to documentation, used modules should also be reloaded
    $(document).foundation(
    {
        "magellan-expedition":
        {
            active_class: 'active', // specify the class used for active sections
            threshold: 1, // how many pixels until the magellan bar sticks, 0 = auto
            destination_threshold: 50, // pixels from the top of destination for it to be considered active
            throttle_delay: 50, // calculation throttling to increase framerate
            fixed_top: 45, // top distance in pixels assigend to the fixed element on scroll
            offset_by_height: true // whether to offset the destination by the expedition height. Usually you want this to be true, unless your expedition is on the side.
        }
    });
};

},{}],3:[function(require,module,exports){
"use strict";

// Load phonegap script
exports.loadPhonegapScript = function(cb)
{
    $.getScript("/phonegap.js", cb);
};

},{}],4:[function(require,module,exports){
"use strict";

// This script is called every single time the page is scrolled by any amount.
// It takes an element to look for and once its scrolled into view calls
// a callback function usually to begin loading the actual element after which
// this will detach and not cal the cb anymore

// elStr is a jquery dom query string
// handler is the function that calls this function on every scroll event
//  until its scrolled into view which it then is disconnected from
// cbFunc is called once its scrolled into view
// firstTime tells the script to do initial setup, it won't be called again
module.exports = function(elStr, handler, cbFunc, firstTime)
{
    // If first time setup is enabled
    if(firstTime === true)
    {
        // Check to see if the element its even on the page
        // Useless to setup an event handler if it isn't
        if($(elStr).length > 0)
        {
            // Set up an event handler
            $(document).on("scroll", handler);
        }

        // Its not on the page, so return without setting anything up
        // Since this func is only called once, by returning without
        // setting up an event handler, it wont be called again
        else return;
    }

    // Now with setup out of the way, lets check to see if its in the viewport
    // this will also be called on initial setup automatically in case it was in
    // the viewport to begin with  and a scrll event hasn't happened yet
    if(require("./scroll-wait")(elStr))
    {
        // Perform loading function and then disable handler as its not needed anymore
        cbFunc();
        $(document).off("scroll", handler);
    }
};

},{"./scroll-wait":5}],5:[function(require,module,exports){
"use strict";

// This returns true or false whether something is in the viewport or not
// Allowing us to hold off execution or scripts until its needed at all
// Significantly increases page load speed
module.exports = function(elem)
{
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
};

},{}],6:[function(require,module,exports){
"use strict";

// Load Twitter Script and call callback if present
exports.loadCodePenScripts = function()
{
    $.getScript('//assets.codepen.io/assets/embed/ei.js');
};

exports.loadCodePenYet = function doLoadCodePenYet(firstTime)
{
    require("../optimizations/load-yet")(".codepen", doLoadCodePenYet, exports.loadCodePenScripts, firstTime);
};

},{"../optimizations/load-yet":4}],7:[function(require,module,exports){
"use strict";

// Injects Facebook Social Share buttons into window
exports.loadFBScript = function(cb)
{
    // Load Facebook Script
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

    // Initialize Facebook script and invoke callback if present
    window.fbAsyncInit = function()
    {
        FB.init({
          appId      : '742572685851625',
          xfbml      : true,  // We do use facebook custom tags so go ahead and auto-init them on page
          version    : 'v2.3'
        });

        if(cb) cb();
    };
};

// This replaces the contents of facebook html tag (usually the facebook icon) with the actual facebook tag markup
exports.insertFBButtons = function()
{
    $(".social-share .facebook").html(
        "<div class='fb-like' data-href='" + window.location.href  + "' data-layout='button_count' data-action='like' data-show-faces='true' data-share='true'></div>"
    );
};

// Insert tags and load up facebook script
exports.socialCycleFB = function()
{
    exports.insertFBButtons();
    exports.loadFBScript();
};

},{}],8:[function(require,module,exports){
"use strict";

// Load Google Plus script and call callback if present
exports.loadGoogleScript = function(cb)
{
    // Load Google Plus script
    (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    if(cb) cb();
};

// Replace dummy google plus html with the actual google plus markup
exports.insertGoogleButtons = function()
{
    $(".social-share .google").html(
        "<div class='g-plusone' data-size='medium' data-href='" + window.location.href  + "'></div>" +
        "<div class='g-plus' data-action='share' data-annotation='bubble' data-href='" + window.location.href  + "'></div>"
    );
};

// Insert Google Plus and Load script
exports.socialCycleGoogle = function()
{
    exports.insertGoogleButtons();
    exports.loadGoogleScript();
};

},{}],9:[function(require,module,exports){
"use strict";

// Load pintrest script and call callback on completion if present
exports.loadPintrestScript = function(cb)
{
    $.getScript("//assets.pinterest.com/js/pinit.js", cb);
};

// Insert pintrest markup replacing dummy one
exports.insertPintrestButtons = function()
{
    $(".social-share .pintrest").html(
        "<a href='//www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location.href) + "&media=" + encodeURIComponent($('meta[property="og:image"]').attr('content')) + "&description=" + encodeURIComponent($('meta[property="og:description"]').attr("content")) + "' data-pin-do='buttonPin' data-pin-config='beside' data-pin-color='red'>" +
            "<img src='//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png'>" +
        "</a>"
    );
};

// Insert button and load up script
exports.socialCyclePintrest = function()
{
    exports.insertPintrestButtons();
    exports.loadPintrestScript();
};

},{}],10:[function(require,module,exports){
"use strict";

// Load Twitter Script and call callback if present
exports.loadTwitterScript = function(cb)
{
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

    if(cb) cb();
};

// Replace twitter dummy content with actual twitter social markup
exports.insertTwitterButtons = function()
{
    $(".social-share .twitter").html(
        "<a class='twitter-share-button' href='https://twitter.com/share' data-url='" + window.location.href  + "' data-via='aoi_ika_desu'>Tweet</a>"
    );
};

// Insert buttons and call script in one shot
exports.socialCycleTwitter = function()
{
    exports.insertTwitterButtons();
    exports.loadTwitterScript();
};

},{}],11:[function(require,module,exports){
"use strict";

// Load Twitter Script and call callback if present
exports.loadYoutubeScripts = function()
{
    $("#youtube-placeholder").html("<iframe class='youtube' width='560' height='315' src='" + $("#youtube-placeholder").attr("data-address") + "' frameborder='0' allowfullscreen></iframe>");
};

exports.loadYoutubeYet = function doLoadYoutubeYet(firstTime)
{
    require("../optimizations/load-yet")("#youtube", doLoadYoutubeYet, exports.loadYoutubeScripts, firstTime);
};

},{"../optimizations/load-yet":4}],12:[function(require,module,exports){
"use strict";

// Load google analytics code
exports.analytics = function()
{
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-63744139-1', 'auto');
    ga('require', 'linkid', 'linkid.js');
    ga('send', 'pageview');
};

},{}],13:[function(require,module,exports){
"use strict";

// Load chikta ads
exports.chiktaAds = function(cb)
{
    $.getScript("//cdn.chitika.net/getads.js");
};

exports.loadChiktaAdsYet = function doLoadChiktaAdsYet(firstTime)
{
    require("../optimizations/load-yet")("#ads", doLoadChiktaAdsYet, exports.chiktaAds, firstTime);
};

},{"../optimizations/load-yet":4}],14:[function(require,module,exports){
"use strict";

/* * * CONFIGURATION VARIABLES * * */
// Load them globally
window.disqus_shortname = null;
window.disqus_identifier = null;
window.disqus_title = null;
window.disqus_url = null;

// load disqus script
exports.loadDisqusScripts = function(cb)
{
    window.disqus_shortname = 'simplecsshacks';

    window.disqus_identifier = $('meta[name="pageid"]').attr('content');
    if(!window.disqus_identifier) window.disqus_identifier = window.location.href;

    window.disqus_title = $('meta[property="og:title"]').attr('content');
    window.disqus_url = $('meta[property="og:url"]').attr('content');

    $.getScript('//' + window.disqus_shortname + '.disqus.com/embed.js', cb);
    $.getScript('//' + window.disqus_shortname + '.disqus.com/count.js');
};

exports.loadDisqusYet = function doLoadDisqusYet(firstTime)
{
    require("../optimizations/load-yet")("#disqus", doLoadDisqusYet, exports.loadDisqusScripts, firstTime);
};

},{"../optimizations/load-yet":4}]},{},[1]);
