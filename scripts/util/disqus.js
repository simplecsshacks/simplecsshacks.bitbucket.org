"use strict";

/* * * CONFIGURATION VARIABLES * * */
// Load them globally
window.disqus_shortname = null;
window.disqus_identifier = null;
window.disqus_title = null;
window.disqus_url = null;

// load disqus script
exports.loadDisqusScripts = function(cb)
{
    window.disqus_shortname = 'simplecsshacks';

    window.disqus_identifier = $('meta[name="pageid"]').attr('content');
    if(!window.disqus_identifier) window.disqus_identifier = window.location.href;

    window.disqus_title = $('meta[property="og:title"]').attr('content');
    window.disqus_url = $('meta[property="og:url"]').attr('content');

    $.getScript('//' + window.disqus_shortname + '.disqus.com/embed.js', cb);
    $.getScript('//' + window.disqus_shortname + '.disqus.com/count.js');
};

exports.loadDisqusYet = function doLoadDisqusYet(firstTime)
{
    require("../optimizations/load-yet")("#disqus", doLoadDisqusYet, exports.loadDisqusScripts, firstTime);
};
