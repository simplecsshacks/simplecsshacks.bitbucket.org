"use strict";

// Load chikta ads
exports.chiktaAds = function(cb)
{
    $.getScript("//cdn.chitika.net/getads.js");
};

exports.loadChiktaAdsYet = function doLoadChiktaAdsYet(firstTime)
{
    require("../optimizations/load-yet")("#ads", doLoadChiktaAdsYet, exports.chiktaAds, firstTime);
};
