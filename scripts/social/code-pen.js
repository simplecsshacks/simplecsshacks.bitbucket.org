"use strict";

// Load Twitter Script and call callback if present
exports.loadCodePenScripts = function()
{
    $.getScript('//assets.codepen.io/assets/embed/ei.js');
};

exports.loadCodePenYet = function doLoadCodePenYet(firstTime)
{
    require("../optimizations/load-yet")(".codepen", doLoadCodePenYet, exports.loadCodePenScripts, firstTime);
};
