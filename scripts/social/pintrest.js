"use strict";

// Load pintrest script and call callback on completion if present
exports.loadPintrestScript = function(cb)
{
    $.getScript("//assets.pinterest.com/js/pinit.js", cb);
};

// Insert pintrest markup replacing dummy one
exports.insertPintrestButtons = function()
{
    $(".social-share .pintrest").html(
        "<a href='//www.pinterest.com/pin/create/button/?url=" + encodeURIComponent(window.location.href) + "&media=" + encodeURIComponent($('meta[property="og:image"]').attr('content')) + "&description=" + encodeURIComponent($('meta[property="og:description"]').attr("content")) + "' data-pin-do='buttonPin' data-pin-config='beside' data-pin-color='red'>" +
            "<img src='//assets.pinterest.com/images/pidgets/pinit_fg_en_rect_red_20.png'>" +
        "</a>"
    );
};

// Insert button and load up script
exports.socialCyclePintrest = function()
{
    exports.insertPintrestButtons();
    exports.loadPintrestScript();
};
