"use strict";

// Injects Facebook Social Share buttons into window
exports.loadFBScript = function(cb)
{
    // Load Facebook Script
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

    // Initialize Facebook script and invoke callback if present
    window.fbAsyncInit = function()
    {
        FB.init({
          appId      : '742572685851625',
          xfbml      : true,  // We do use facebook custom tags so go ahead and auto-init them on page
          version    : 'v2.3'
        });

        if(cb) cb();
    };
};

// This replaces the contents of facebook html tag (usually the facebook icon) with the actual facebook tag markup
exports.insertFBButtons = function()
{
    $(".social-share .facebook").html(
        "<div class='fb-like' data-href='" + window.location.href  + "' data-layout='button_count' data-action='like' data-show-faces='true' data-share='true'></div>"
    );
};

// Insert tags and load up facebook script
exports.socialCycleFB = function()
{
    exports.insertFBButtons();
    exports.loadFBScript();
};
