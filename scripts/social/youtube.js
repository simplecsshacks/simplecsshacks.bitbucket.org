"use strict";

// Load Twitter Script and call callback if present
exports.loadYoutubeScripts = function()
{
    $("#youtube-placeholder").html("<iframe class='youtube' width='560' height='315' src='" + $("#youtube-placeholder").attr("data-address") + "' frameborder='0' allowfullscreen></iframe>");
};

exports.loadYoutubeYet = function doLoadYoutubeYet(firstTime)
{
    require("../optimizations/load-yet")("#youtube", doLoadYoutubeYet, exports.loadYoutubeScripts, firstTime);
};
