"use strict";

// Load Twitter Script and call callback if present
exports.loadTwitterScript = function(cb)
{
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');

    if(cb) cb();
};

// Replace twitter dummy content with actual twitter social markup
exports.insertTwitterButtons = function()
{
    $(".social-share .twitter").html(
        "<a class='twitter-share-button' href='https://twitter.com/share' data-url='" + window.location.href  + "' data-via='aoi_ika_desu'>Tweet</a>"
    );
};

// Insert buttons and call script in one shot
exports.socialCycleTwitter = function()
{
    exports.insertTwitterButtons();
    exports.loadTwitterScript();
};
