"use strict";

// Load Google Plus script and call callback if present
exports.loadGoogleScript = function(cb)
{
    // Load Google Plus script
    (function() {
    var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
    po.src = 'https://apis.google.com/js/platform.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
    })();

    if(cb) cb();
};

// Replace dummy google plus html with the actual google plus markup
exports.insertGoogleButtons = function()
{
    $(".social-share .google").html(
        "<div class='g-plusone' data-size='medium' data-href='" + window.location.href  + "'></div>" +
        "<div class='g-plus' data-action='share' data-annotation='bubble' data-href='" + window.location.href  + "'></div>"
    );
};

// Insert Google Plus and Load script
exports.socialCycleGoogle = function()
{
    exports.insertGoogleButtons();
    exports.loadGoogleScript();
};
