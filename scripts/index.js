"use strict";

function phonegapBoot()
{
    $(".browsing-mode").html("<span class='im im-phone3'></span> Phonegap Mode");
}

var ranOnce = false;

// This was made for the cases where libraries would be seperate from site code
// and both loaded asynchronusly. One or the other would load first due to race-conditions.
// This fixed that by attaching a special code at the end of both files to call runOnce.

// This is no longer the case however as all files are compacted to one file, but this is
// kept in anyways in case the files are split sometime in the future again
function runOnce()
{
    //console.log("runOnce called");

    if(!ranOnce) ranOnce = true;
    else return;

    $(".social-share .facebook").click(require("./social/facebook").socialCycleFB);
    $(".social-share .twitter").click(require("./social/twitter").socialCycleTwitter);
    $(".social-share .google").click(require("./social/google-plus").socialCycleGoogle);
    $(".social-share .pintrest").click(require("./social/pintrest").socialCyclePintrest);

    require("./util/chikta-ads").loadChiktaAdsYet(true);
    require("./util/disqus").loadDisqusYet(true);
    require("./social/code-pen").loadCodePenYet(true);
    require("./social/youtube").loadYoutubeYet(true);

    require("./lib/foundation").reloadFoundation();
    require("./util/analytics").analytics();
    require("./lib/phonegap").loadPhonegapScript();

    if(
        window.nwDispatcher &&
        window.nwDispatcher.requireNwGui) $(".browsing-mode").html("<span class='im im-desktop2'></span> Node-Webkit Mode");
    else $(".browsing-mode").html("<span class='im im-browser3'></span> Web Browser Mode");

    // Listen to this to detect if we're running inside phonegap or not
    $(document).on("deviceready", phonegapBoot);
}

window.libsReady = function()
{
    runOnce();
};

if(typeof $ != 'undefined')
{
    $(function()
    {
        //console.log("jQuery onReady called");
        runOnce();
    });
}
