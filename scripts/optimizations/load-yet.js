"use strict";

// This script is called every single time the page is scrolled by any amount.
// It takes an element to look for and once its scrolled into view calls
// a callback function usually to begin loading the actual element after which
// this will detach and not cal the cb anymore

// elStr is a jquery dom query string
// handler is the function that calls this function on every scroll event
//  until its scrolled into view which it then is disconnected from
// cbFunc is called once its scrolled into view
// firstTime tells the script to do initial setup, it won't be called again
module.exports = function(elStr, handler, cbFunc, firstTime)
{
    // If first time setup is enabled
    if(firstTime === true)
    {
        // Check to see if the element its even on the page
        // Useless to setup an event handler if it isn't
        if($(elStr).length > 0)
        {
            // Set up an event handler
            $(document).on("scroll", handler);
        }

        // Its not on the page, so return without setting anything up
        // Since this func is only called once, by returning without
        // setting up an event handler, it wont be called again
        else return;
    }

    // Now with setup out of the way, lets check to see if its in the viewport
    // this will also be called on initial setup automatically in case it was in
    // the viewport to begin with  and a scrll event hasn't happened yet
    if(require("./scroll-wait")(elStr))
    {
        // Perform loading function and then disable handler as its not needed anymore
        cbFunc();
        $(document).off("scroll", handler);
    }
};
