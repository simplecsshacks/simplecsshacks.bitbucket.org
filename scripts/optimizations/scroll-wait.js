"use strict";

// This returns true or false whether something is in the viewport or not
// Allowing us to hold off execution or scripts until its needed at all
// Significantly increases page load speed
module.exports = function(elem)
{
    var $elem = $(elem);
    var $window = $(window);

    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();

    var elemTop = $elem.offset().top;
    var elemBottom = elemTop + $elem.height();

    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
};
